from django.shortcuts import render
from labtest.models import Test
from pdf.models import AdminReport
from pdf.views import generate_adminreport, generate_testcountreport

# Create your views here.
def reports(request):
    date_from = request.GET.get('date-from')
    date_to = request.GET.get('date-to')
    report_type = request.GET.get('report-type')
    if request.method == "GET"\
       and date_from != ""\
           and date_to != ""\
               and report_type in ( "admin-report", "test-count-report" ):
        from django.utils.timezone import datetime
        date_from = datetime.strptime(date_from, "%Y-%m-%d")
        date_to = datetime.strptime(date_to, "%Y-%m-%d")
        tests = Test.objects.filter(date_requested__range=[date_from, date_to])

        if report_type == "admin-report":
            return generate_adminreport(request, tests, date_from, date_to)
        else: return generate_testcountreport(request, tests, date_from, date_to)
    return render(request, "reports.html", { "date_to": date_to, "date_from": date_from })


def doctorReport(request, dr):
    if request.method == "POST":
        date_from = request.POST.get('date-from')
        date_to = request.POST.get('date-to')
        from django.utils.timezone import datetime
        date_from = datetime.strptime(date_from, "%Y-%m-%d")
        date_to = datetime.strptime(date_to, "%Y-%m-%d")
        tests = Test.objects.filter(date_requested__range=[date_from, date_to],
                                    ref_by__id=dr)
        return generate_adminreport(request, tests, date_from, date_to)
