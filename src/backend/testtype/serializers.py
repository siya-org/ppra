from .models import TestCategory, TestParticular, TestParticularGroup
from rest_framework import serializers


class TestCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TestCategory
        fields = ['id', 'name']

class TestParticularSerializer(serializers.HyperlinkedModelSerializer):
    category = serializers.PrimaryKeyRelatedField(queryset=TestCategory.objects.all())

    class Meta:
        model = TestParticular
        fields = ['id', 'name', 'unit', 'ref_value', 'category', 'value_type', 'ref_type', 'groups']
        read_only_fields = ('id',)


class TestParticularGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TestParticularGroup
        fields = ['id', 'name', 'price']
