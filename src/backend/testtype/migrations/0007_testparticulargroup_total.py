# Generated by Django 3.0.2 on 2020-01-28 12:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testtype', '0006_auto_20200128_0535'),
    ]

    operations = [
        migrations.AddField(
            model_name='testparticulargroup',
            name='total',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
