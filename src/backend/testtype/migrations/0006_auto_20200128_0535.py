# Generated by Django 3.0.2 on 2020-01-28 05:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testtype', '0005_testparticular_method'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testparticular',
            name='method',
            field=models.CharField(default='N/A', max_length=255),
        ),
    ]
