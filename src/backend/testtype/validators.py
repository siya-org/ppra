from django.core.exceptions import ValidationError

possible_value_types = [
    "number",
    "array",
    "boolean"
]
possible_ref_types = [
    "number",
    "array",
    "boolean",
    "range"
]


def validate_value_type(value):
    if value in possible_value_types: return value
    else:
        raise ValidationError(
            "'{}' is not a valid value type. Possible value types are: {}"
            .format(value,
                    ", ".join(possible_value_types)))


def validate_ref_type(value):
    if value in possible_ref_types: return value
    else:
        raise ValidationError(
            "'{}' is not a valid reference type. Possible reference types are: {}"
            .format(value,
                    ", ".join(possible_ref_types)))
