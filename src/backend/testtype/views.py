from django.shortcuts import render
from .models import TestCategory, TestParticular, TestParticularGroup, TestParticularGroupPrice
from .serializers import TestCategorySerializer, TestParticularSerializer, TestParticularGroupSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from jwtauth.models import IsAdminOrReadOnlyForAuthenticated
from django.views.generic.edit import DeleteView, UpdateView
from django.views.generic import ListView
from django.urls import reverse_lazy
# Create your views here.

def get_ref_value(ref_value, ref_type):
    import re
    comparator_re = re.compile("^[ ]*[><=][ ]*[=]?[ ]*([0-9]+)")
    list_re = re.compile("([^,]+)(,[^,]+)+")
    range_re = re.compile("([0-9]+)\-([0-9]+)")
    boolean_re = re.compile("([^\|]+)\|([^\|])")

    if ref_type == 'comparator':
        return comparator_re.match(ref_value).groups()
    if ref_type == 'range':
         return range_re.match(ref_value).groups()
    if ref_type == 'list':
        return list_re.match(ref_value).groups()
    if ref_type == 'boolean':
        return boolean_re.match(ref_value).groups()

def get_ref_type(ref_value):
    # range
    # boolean
    # list
    # comparator
    import re
    comparator_re = re.compile("^[ ]*[><=][ ]*[=]?[ ]*([0-9]+)")
    list_re = re.compile("([^,]+)(,[^,]+)+")
    range_re = re.compile("([0-9]+)\-([0-9]+)")
    boolean_re = re.compile("([^\|]+)\|([^\|])")

    if (comparator_re.match(ref_value)):
        return 'comparator'
    elif (list_re.match(ref_value)):
        return 'list'
    elif (boolean_re.match(ref_value)):
        return 'boolean'
    elif (range_re.match(ref_value)):
        return 'range'

class TestCategoryViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = [IsAdminOrReadOnlyForAuthenticated]

    queryset = TestCategory.objects.all()
    serializer_class = TestCategorySerializer

class TestParticularViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = [IsAdminOrReadOnlyForAuthenticated]

    queryset = TestParticular.objects.all()
    serializer_class = TestParticularSerializer


class TestParticularGroupViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = [IsAdminOrReadOnlyForAuthenticated]

    queryset = TestParticularGroup.objects.all()
    serializer_class = TestParticularGroupSerializer


def addTestType(request):
    if request.method == 'POST':
        acronym = request.POST.get('test-acronym')
        name = request.POST.get('test-name')
        unit = request.POST.get('test-unit')
        ref_value = request.POST.get('test-ref-value')
        category = request.POST.get('test-category')
        method = request.POST.get('test-method')
        calculated_value = request.POST.get('test-calculated-value')
        group_prices = [{'group': _, 'price': request.POST.get('test-' + str(_.id) + '-price', None)} for _ in TestParticularGroup.objects.all()]
        particular = TestParticular.objects.create(
            acronym = acronym,
            name = name,
            unit = unit,
            ref_value = ref_value,
            category = TestCategory.objects.get(id=category),
            calculated_value = calculated_value,
            method=method
        )
        gps = [TestParticularGroupPrice.objects.create(group=group['group'],
                                                 price=int(group['price']))
               for group in group_prices if group['price'] != '']
        particular.groups.set(gps)
    return render(request, 'add_testtype.html', {
        'groups': TestParticularGroup.objects.all(),
        'categories': TestCategory.objects.all(),
        'testtypes': TestParticular.objects.all().order_by("-id")[:5]
    })

def addTestCategory(request):
    if request.method == "POST":
        category_name = request.POST.get("category-name")
        category_color = request.POST.get("category-color")
        TestCategory.objects.create(name=category_name, colour=category_color)
    return render(request, 'add_testcategory.html', {
        'categories': TestCategory.objects.all()
    })


def addTestGroup(request):
    if request.method == "POST":
        group_name = request.POST.get("group-name")
        group_total = request.POST.get("group-total")
        if group_total == '': group_total = None
        TestParticularGroup.objects.create(name=group_name, total=group_total)
    return render(request, 'add_testgroup.html', {
        'groups': TestParticularGroup.objects.all()
    })


class TestCategoryDelete(DeleteView):
    template_name = 'test_category_delete_confirm.html'
    model = TestCategory
    success_url = reverse_lazy('addTestCategory')

class TestGroupDelete(DeleteView):
    template_name = 'test_group_delete_confirm.html'
    model = TestParticularGroup
    success_url = reverse_lazy('addTestGroup')


class TestTypeDelete(DeleteView):
    template_name = 'test_particular_delete_confirm.html'
    model = TestParticular
    success_url = reverse_lazy('testtypes')


class TestCategoryUpdate(UpdateView):
    model = TestCategory
    fields = ['name', 'colour']
    template_name = 'test_category_update.html'
    success_url = reverse_lazy('addTestCategory')


class TestGroupUpdate(UpdateView):
    model = TestParticularGroup
    fields = ['name', 'total', 'default']
    template_name = 'test_group_update.html'
    success_url = reverse_lazy('addTestGroup')


class TestTypeUpdate(UpdateView):
    model = TestParticular
    fields = [
        'acronym',
        'name',
        'unit',
        'ref_value',
        'category',
        'calculated_value',
    ]
    template_name = 'test_particular_update.html'
    success_url = reverse_lazy('addTestType')


class TestParticularGroupPriceUpdate(UpdateView):
    model = TestParticularGroupPrice
    fields = ['price', 'group']
    template_name = 'test_particular_group_price_update.html'
    success_url = reverse_lazy("addTestType")


class TestTypeList(ListView):
    queryset = TestParticular.objects.all().order_by("-id")
    template_name = 'testtypes.html'
    paginate_by = 20
