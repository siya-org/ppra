from django.db import models
import re

import random

from .validators import validate_value_type, validate_ref_type

# Create your models here.
class TestCategory(models.Model):
    name = models.CharField(max_length=255)
    colour = models.CharField(max_length=11, default="255,255,255")

    def should_calculate_total(self):
        return True if self.total else False

    def save(self, *args, **kwargs):
        if self.colour == "255,255,255":
            self.colour = "{},{},{}".format(
                str(random.randint(50, 255)),
                str(random.randint(50, 255)),
                str(random.randint(50, 255)))
        super().save(*args, **kwargs)

    def __str__(self):
        return "{}. {}".format(self.id, self.name)

class TestParticularGroup(models.Model):
    name = models.CharField(max_length=255)
    total = models.IntegerField(blank=True, null=True)
    default = models.BooleanField(default=False)

    def apply_total(self):
        return True if self.total else False

    def __str__(self): return self.name

class TestParticularGroupPrice(models.Model):
    group = models.ForeignKey(TestParticularGroup, default=None, on_delete=models.CASCADE)
    price = models.IntegerField(default=-1, blank=True)
    def __str__(self): return self.group.name


class TestParticular (models.Model):
    acronym = models.CharField(max_length=255, default="")
    name = models.CharField(max_length=255)
    unit = models.CharField(max_length=255, blank=True, default="")
    method = models.CharField(max_length=255, default="N/A")
    '''
    ** Note on ref_value:
    | Type    | Example                | Description                           |
    |---------+------------------------+---------------------------------------|
    | Number  | "123.45"               | Get number from string                |
    | Boolean | "Is White,Isn't White" | CSV values, [0] -> True, [1] -> False |
    | Array   | "Yellow,Brown,Red"     | CSV values                            |
    | Range   | "4,10", ",7", "8,"     | [0] -> lowr bound, [1] -> upper bound |
    |         |                        | Either first or second field can be   |
    |         |                        | empty                                 |
    |---------+------------------------+---------------------------------------|
    '''
    ref_value = models.CharField(max_length=255, default="")
    calculated_value = models.CharField(max_length=255, default="", blank=True, null=True)
    category = models.ForeignKey(TestCategory, on_delete=models.CASCADE)
    ref_type = models.CharField(max_length=255,
                                validators=[validate_ref_type])
    groups = models.ManyToManyField(TestParticularGroupPrice, default=None)
    def __str__(self): return "{} in ({})".format(self.name,
                                                self.unit)


    def depends_on(self):
        if not self.is_calculated(): return None
        from string import Formatter
        deps = map( lambda l: int(l.split("-")[1]), (_[1] for _ in Formatter().parse(self.calculated_value) if _[1] is not None))
        return TestParticular.objects.filter(id__in=(deps))

    def depends_on_json(self):
        deps = self.depends_on()
        if deps is None: return []
        import json
        return json.dumps([ _.id for _ in deps])

    def is_calculated(self):
        return not (self.calculated_value in [None, ""])

    def save(self, *args, **kwargs):
        self.ref_type = self.get_ref_type()
        super().save(*args, **kwargs)


    def get_ref_value_for_display(self, grp):
        op = self.get_ref_op(grp)
        if op and op.startswith("list/"):
            return self.get_ref_op(grp).split("/")[1]
        else:
            return self.get_ref_value(grp)

    def get_ref_value(self, grp):
        if self.ref_type == None or self.ref_value == None:
            return None
        ref_value = self.get_ref_comp(grp)
        ref_type = self.ref_type
        import re
        comparator_re = re.compile("^[ ]*([><=][ ]*[=]?)[ ]*([0-9:]+\.?[0-9]*)")
        list_re = re.compile("([^,]+)((,[^,]+)+)")
        range_re = re.compile("([0-9]+\.?[0-9]*)\-([0-9]+\.?[0-9]*)")
        range_re = re.compile("([0-9]+\.?[0-9]*)\-([0-9]+\.?[0-9]*)")
        boolean_re = re.compile("([^\|]+)\|([^\|])")

        if ref_type == 'comparator':
            return re.findall(comparator_re, ref_value)[0]
        if ref_type == 'range':
            return ref_value.split("-")
        if ref_type == 'list' or ref_type == "multilist":
            op = self.get_ref_op(grp)
            list_vals = ref_value.split(",")
            return list(map( lambda v: v.strip( ","), list_vals))
        if ref_type == 'boolean':
            return ref_value.split("|")
        return None

    def get_ref_type(self):
        if self.ref_type == None or self.ref_value == None:
            return None
        import re
        ref_val = self.get_ref_comp(self.get_groups()[0])
        comparator_re = re.compile("^[ ]*[><=][ ]*[=]?[ ]*([0-9:]+\.?[0-9]*)")
        list_re = re.compile("([^,]+)(,[^,]+)+")
        range_re = re.compile("([0-9]+\.?[0-9]*)\-([0-9]+\.?[0-9]*)")
        range_re = re.compile("([0-9]+\.?[0-9]*)\-([0-9]+\.?[0-9]*)")
        boolean_re = re.compile("([^\|]+)\|([^\|])")
        if comparator_re.match(ref_val): return "comparator"
        elif list_re.match(ref_val):
            op = self.get_ref_op(self.get_groups()[0])
            if (op and op.lower() == "multi"):
                return "multilist"
            return "list"
        elif range_re.match(ref_val): return "range"
        elif boolean_re.match(ref_val): return "boolean"
        else: return None

    def get_ref_op(self, grp):
        p = re.compile("([a-zA-Z0-9\:\.\/-]*)\([^\(\)]*\)")
        m = p.match(grp).groups()[0]
        if m != '': return m
        return None

    def get_ref_ops(self):
        import json
        return json.dumps([self.get_ref_op(_) for _ in self.get_groups()])

    def get_groups(self):
        p = re.compile("([a-zA-Z0-9\:\.\/-]*\([^\(\)]*\))")
        return re.findall(p, self.ref_value)

    def get_ref(self):
        valid_group = self.get_valid_group()
        op = self.get_ref_op(valid_group)
        if (op is not None and op.startswith("list/")):
            val = self.get_ref_value(valid_group)
        else:
            val = self.get_ref_comp(valid_group)


    def get_ref_values(self):
        return list(map(lambda x: self.get_ref_value(x), self.get_groups()))

    def get_ref_values_for_display(self):
        return list(map(lambda x: self.get_ref_value_for_display(x), self.get_groups()))

    def get_ref_values_for_display_as_json(self):
        import json
        return json.dumps(self.get_ref_values())

    def get_ref_values_as_json(self):
        import json
        return json.dumps(self.get_ref_values())

    def get_ref_comp(self, grp):
        p = re.compile("[a-zA-Z0-9:\.\/-]*\(([^\(\)]*)\)")
        m = p.match(grp).groups()[0]
        if m != '': return m
        return None

    def is_op_attr(self, grp):
        op = self.get_ref_op(grp)
        if op and ":" in op:
            return True
        return False
