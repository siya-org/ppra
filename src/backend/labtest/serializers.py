from .models import Test, TestParticularValue
from rest_framework import serializers

from patient.models import Patient
from lab.models import Lab
from testtype.models import TestCategory, TestParticular

from .models import Test

class TestSerializer(serializers.ModelSerializer):
    # patient = serializers.PrimaryKeyRelatedField(queryset=Patient.objects.all())
    # lab = serializers.PrimaryKeyRelatedField(queryset=Lab.objects.all())
    # categories = serializers.PrimaryKeyRelatedField(queryset=TestCategory.objects.all(), many=True)
    class Meta:
        model = Test
        fields = ['id', 'patient', 'lab', 'state', 'ref_by', 'investigation_required', 'categories', 'date_requested']
        read_only_fields = ('id',)

    def create(self, validated_data):
        cats = validated_data.pop("categories")
        test = Test.objects.create(**validated_data)
        for c in cats: test.categories.add(c)
        for category in test.categories.all():
            tp = TestParticular.objects.filter(category=category)
            for particular in tp:
                pv, created = TestParticularValue.objects.get_or_create(particular=particular,
                                                                        is_valid=True,
                                                                        test=test)
                if created: pv.save()
        return test



class TestParticularValueSerializer(serializers.HyperlinkedModelSerializer):
    particular = serializers.PrimaryKeyRelatedField(queryset=TestParticular.objects.all())
    test = serializers.PrimaryKeyRelatedField(queryset=Test.objects.all())
    class Meta:
        model = TestParticularValue
        fields = ['id', 'particular', 'value', 'test', 'is_valid']
        read_only_fields = ('id',)
