from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from testtype.models import TestCategory, TestParticular, TestParticularGroup
from patient.models import Patient
from lab.models import Lab
from bill.models import Bill
from django.db.models import Q

# Create your models here.

class Doctor(models.Model):
    name =  models.CharField(max_length=255)

    def __str__(self): return self.name

class Test(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    '''
    0 - Requested
    1 - Processing
    2 - Completed
    '''
    state = models.IntegerField(default=0, validators=[MaxValueValidator(2), MinValueValidator(0)])
    ref_by = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    investigation_required =models.CharField(max_length=255)
    date_requested = models.DateTimeField(auto_now_add=True)
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)

    def __str__(self): return "{} - {}".format(self.patient.name, self.date_requested)


class TestParticularValue(models.Model):
    particular = models.ForeignKey(TestParticular, on_delete=models.CASCADE)
    value = models.CharField(max_length=255, default="")
    is_valid = models.BooleanField(default=True)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    group = models.ForeignKey(TestParticularGroup, blank=True, null=True, on_delete=models.CASCADE)
    price = models.IntegerField(default=0) # price is duplicated here so that future changes to price don't retroactively afftect older tests


    def get_price(self):
        return self.price

    def get_particular_price(self):
        if self.group:
            return self.particular.groups.get(group=self.group).price
        else:
            return self.particular.groups.first().price

    def save(self, *args, **kwargs):
        self.is_valid = self.check_validity()
        super().save(*args, **kwargs)

    def is_applicable(self, group):
        comp = self.particular.get_ref_comp(group)
        if not self.particular.is_op_attr(group): return True
        else:
            # op is attr based
            attr, value = self.particular.get_ref_op(group).split(":")
            if (attr.lower() == "sex") and self.test.patient.sex.lower() == value:
                return True
            return False

    def get_ref(self):
        valid_group = self.get_valid_group()
        op = self.particular.get_ref_op(valid_group)
        if (op and op.startswith("list/")):
            val = self.particular.get_ref_op(valid_group).split("/")[1]
        elif (op and ":" in op):
            val = "{}({})".format(self.particular.get_ref_op(valid_group).split(":")[1], self.particular.get_ref_comp(valid_group))
        else:
            val = self.particular.get_ref_comp(valid_group)
        return val

    def get_valid_group(self):
        lst = list(filter(lambda g: self.is_applicable(g),
                              self.particular.get_groups()))
        if len(lst) == 0: return None
        return lst[0]

    def get_value(self):
        if self.particular.is_calculated():
            return str(self.get_calculated_price())
        op = self.particular.get_ref_op(self.get_valid_group())
        if self.check_validity() and op:
            if op.startswith("list/") or ":" in op:
                return self.value
            return "{}({})".format( op, self.value)
        return self.value

    def get_calculated_price(self):
        if not self.particular.is_calculated(): return ""
        def is_number(s):
            try:
                float(s)
                return True
            except ValueError:
                return False
        l = {"t-" + str(_.particular.id) : _.get_value() for _ in  TestParticularValue.objects.filter(test=self.test).filter(~Q(id=self.id)) if is_number(_.value)}
        return eval(self.particular.calculated_value.format(**l))

    def check_validity(self):
        ref_t = self.particular.get_ref_type()
        if ref_t:
            ref = self.get_valid_group()
            if ref is None: return False
            ref_val = (self.particular.get_ref_value(ref))
            print(ref_val)

            if self.particular.is_calculated():
                value = self.get_calculated_price()
            else:
                value = self.value
            if ref_t == 'range':
                low_val = float(ref_val[0])
                high_val = float(ref_val[1])
                return float(value) >= low_val and float(value) <= high_val
            elif ref_t == 'boolean':
                true_val = ref_val[0].lower()
                false_val = ref_val[1].lower()
                return value.lower() == true_val
            elif ref_t == "multilist":
                return all( el in  self.particular.get_ref_values()[0] for el in value.split(","))
            elif ref_t == 'list':
                return value in self.particular.ref_value
            elif ref_t == 'comparator':
                if ":" in value:
                    from fractions import Fraction
                    value = Fraction(*(int(_) for _ in value.split(":")))

                comp = ref_val[0]
                if  ":" in ref_val[1]:
                    val = ref_val[1]
                    from fractions import Fraction
                    val = Fraction(*(int(_) for _ in val.split(":")))
                else:
                    val = float(ref_val[1])
                if type(value) == str:
                    value = float(value)
                # <, >, <=, >=, =, ==
                # value = test value, val = reference value
                print(value, val, comp, value > val)
                if comp == '<': return (value < val)
                elif comp == '>': return (value > val)
                elif comp == '=': return (value == val)
                elif comp == '<=': return (value <= val)
                elif comp == '>=': return (value >= val)
                elif comp == '==': return (value == val)
                return False
        return False
