from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from .models import Test, TestParticularValue, Doctor
from testtype.models import TestParticular, TestParticularGroup, TestParticularGroupPrice
from .serializers import TestSerializer, TestParticularValueSerializer
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from patient.models import Patient
from lab.models import Lab
from bill.models import Bill, Tax
from django.views import generic
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.http import Http404
# Create your views here.


class TestViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = [IsAuthenticated]

    queryset = Test.objects.all()
    serializer_class = TestSerializer

class TestParticularValueViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = [IsAuthenticated]

    queryset = TestParticularValue.objects.all()
    serializer_class = TestParticularValueSerializer



class TestList(generic.ListView):
    queryset = Test.objects.all().order_by("-date_requested")
    template_name = 'tests.html'
    paginate_by = 10

class TestDetails(generic.DetailView):
    model = Test
    template_name = 'testDetails.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['testParticularValues'] = TestParticularValue.objects.filter(test=self.object)
        due = self.object.bill.due()
        discount = (due % 5)
        if discount < 1: discount = 0
        discount = round(discount)
        context['due_discount'] = ( discount, due - discount )
        return context


def addTest(request):
    if (request.method == "POST"):
        particular_keys = filter(lambda key: "particular" in key, request.POST.keys())
        particulars = list(map( lambda key: {
            'value': ",".join(request.POST.getlist(key)),
            'name' : TestParticular.objects.get(id=key.replace("particular-", ""))
        }, particular_keys))

        group_keys = filter(lambda key: "group-" in key, request.POST.keys())
        groups=[]
        for group_key in group_keys:
            key = int(group_key.replace("group-", ""))
            if key == 0: continue # because: check template
            group = TestParticularGroup.objects.get(id=key)
            groups += [group]
            group_particulars = request.POST.getlist(group_key)
            for p in filter(lambda p: str(p['name'].id) in group_particulars, particulars):
                p['group'] = group
        # CHECK IF GROUP TOTALS
        for group in groups:
            if group.apply_total():
                sum_val = sum(map( lambda p: int(p['value']), filter(lambda p: p.get("group", None) == group, particulars)))
                if sum_val != group.total:
                    request.status_code = 404
                    return render(request, "404.html", {
                        "message": "Total Not Valid! Total should be '{}', but it is '{}' instead.".format(group.total, sum_val)
                    })
                    # raise Http404("Total Not Valid! Total should be '{}', but it is '{}' instead.".format(group.total, sum_val))

        patient = Patient.objects.create(
            name=request.POST.get('patient-name'),
            age=request.POST.get('patient-age'),
            sex=request.POST.get('patient-sex', 'male'),
            address=request.POST.get('patient-address')
        )
        doctor = Doctor.objects.get(id =request.POST.get('ref-by'))
        patient.save()
        lab = Lab.objects.all()[0]


        bill = Bill.objects.create( total=0, paid=0 )

        test = Test.objects.create(
            patient = patient,
            lab = lab,
            state = 0,
            ref_by = doctor,
            investigation_required = ", ".join({_['name'].acronym for _ in particulars}),
            bill=bill
        )
        test.save()
        total_sum = 0
        for particular in particulars:
            tv = TestParticularValue.objects.create(
                particular = particular['name'],
                value = particular['value'],
                test = test,
                group = particular.get("group", None),
            )
            tv.price = tv.get_particular_price()
            total_sum += tv.price
            tv.is_valid = tv.check_validity()
            tv.save()
        bill.total = total_sum
        bill.taxes.set(list(Tax.objects.all()))
        bill.is_paid_off = bill.is_paid()
        bill.save()

        return redirect('test', pk=test.id)

    return render(request, "test.html", {
        "test_particulars": TestParticular.objects.all(),
        "doctors": Doctor.objects.all(),
        "groups": TestParticularGroup.objects.all(),
        "particulars_groups": [
            {
                "group": group,
                "doctors": Doctor.objects.all(),
                "particulars": list(TestParticular.objects.filter(groups__group=group))
            } for group in TestParticularGroup.objects.all()
        ]
    })

class TestDelete(DeleteView):
    template_name = 'test_delete_confirm.html'
    model = Test
    success_url = reverse_lazy('tests')


class DoctorList(generic.ListView):
    queryset = Doctor.objects.all().order_by("name")
    template_name = 'doctors.html'
    paginate_by = 10

class TestsByDr(generic.ListView):

    def get_queryset(self):
        date_from = self.request.GET.get('date-from')
        date_to = self.request.GET.get('date-to')
        if self.request.method == 'GET' and date_from and date_to:
            from datetime import datetime
            date_from = datetime.strptime(date_from, "%Y-%m-%d")
            date_to = datetime.strptime(date_to, "%Y-%m-%d")
            return Test.objects.filter(ref_by__id = self.kwargs['dr']).filter(date_requested__gte=date_from).filter(date_requested__lte=date_to)
        else: return Test.objects.filter(ref_by__id = self.kwargs['dr'])
    template_name = 'tests.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dr'] = Doctor.objects.get(id=self.kwargs['dr'])
        return context

class DoctorDelete(DeleteView):
    template_name = 'doctor_delete_confirm.html'
    model = Doctor
    success_url = reverse_lazy('doctors')


class DoctorCreate(CreateView):
    template_name = 'doctor_create.html'
    model = Doctor
    fields = ['name']
    success_url = reverse_lazy("doctors")

class DoctorUpdate(UpdateView):
    template_name = 'doctor_update.html'
    model = Doctor
    fields = ['name']
    success_url = reverse_lazy("doctors")

class TestParticularValueUpdate(UpdateView):
    template_name = "testparticularvalue_update.html"
    model = TestParticularValue
    fields = ['value']
    success_url = reverse_lazy("addTestType")


class PaidTestList(generic.ListView):
    queryset = Test.objects.filter(bill__is_paid_off=True).order_by("-date_requested")
    template_name = 'tests.html'
    paginate_by = 10

class UnpaidTestList(generic.ListView):
    queryset = Test.objects.filter(bill__is_paid_off=False).order_by("-date_requested")
    template_name = 'tests.html'
    paginate_by = 10
