# Generated by Django 3.0.2 on 2020-02-06 09:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lab', '0005_auto_20200122_0609'),
        ('labtest', '0008_auto_20200206_0911'),
    ]

    operations = [
        migrations.AlterField(
            model_name='test',
            name='lab',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lab.Lab'),
        ),
    ]
