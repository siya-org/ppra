# Generated by Django 3.0.2 on 2020-01-17 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient', '0007_remove_patient_referenced_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='address',
            field=models.CharField(default='N/A', max_length=255),
        ),
    ]
