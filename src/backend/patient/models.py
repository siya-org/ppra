from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Patient(models.Model):
    name = models.CharField(max_length=255, default="N/A")
    age = models.IntegerField(default=-1,
                              validators=[
                                  MaxValueValidator(100),
                                  MinValueValidator(-1)
                              ])
    sex = models.CharField(max_length=10, default="N/A")
    address = models.CharField(max_length=255, default="N/A")

    def __str__(self):
        return "{}. {}".format(self.id, self.name)
