from .models import Patient
from rest_framework import serializers


class PatientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Patient
        fields = ['id', 'name', 'age', 'patient_id', 'sex', 'address']
        read_only_fields = ('id',)
