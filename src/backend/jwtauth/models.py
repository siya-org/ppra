from django.db import models

# Create your models here.

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from rest_framework import permissions

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class IsAdminOrReadOnlyForAuthenticated(permissions.BasePermission):
    '''
    Either user is admin, or it's a safe request
    '''
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if (request.method in permissions.SAFE_METHODS) and request.user.is_authenticated:
            return True

        # Instance must have an attribute named `owner`.
        return request.user.is_staff
