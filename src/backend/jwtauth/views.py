from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model # If used custom user model
from .serializers import UserSerializer
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAdminUser


class CreateUserView(CreateAPIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAdminUser]

    model = User

    queryset = User.objects.all()

    serializer_class = UserSerializer
