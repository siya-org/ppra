from django.shortcuts import render
from .models import Lab
from .serializers import LabSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import render
from rest_framework import viewsets

# Create your views here.


class LabViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = [IsAuthenticated]

    queryset = Lab.objects.all()
    serializer_class = LabSerializer


def home(request):
    home_template = "home.html"
    return render(request, home_template)

def labs(request):
    home_template = "labs.html"
    return render(request, home_template, { 'labs': Lab.objects.all() })
