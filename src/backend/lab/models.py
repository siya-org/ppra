from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Lab(models.Model):
    name = models.CharField(max_length=255, default="N/A")
    logo  = models.ImageField(upload_to="static/images/", blank=True)
    reg_no = models.CharField(max_length=255, default="N/A")
    council_reg_no = models.CharField(max_length=255, default="N/A")
    mold_reg_no  = models.CharField(max_length=255, default="N/A")
    pan_no = models.IntegerField(default=-1)
    description = models.TextField()
    moto = models.CharField(max_length=255, default="")

    def _str__(self):
        return "{} - Reg. No. - {}".format(self.name, self.reg_no)
