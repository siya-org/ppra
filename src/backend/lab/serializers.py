from .models import Lab
from rest_framework import serializers


class LabSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lab
        fields = ['name','logo', 'reg_no', 'council_reg_no', 'mold_reg_no', 'pan_no', 'description', 'moto']
