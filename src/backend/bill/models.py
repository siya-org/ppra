from django.db import models

# Create your models here.
class Tax(models.Model):
    name = models.CharField(max_length=255)
    method = models.CharField(max_length=10) # percentage / fixed
    value = models.IntegerField()
    def __str__(self): return self.name

    def get_value_for_display(self):
        if self.method == "percentage":
            return " {}%".format(str(self.value))
        else:
            return "Rs. " + str(self.value)

    def get_value(self, val):
        if self.method == "percentage":
            return val * (self.value / 100)
        return self.value



class Bill(models.Model):
    total = models.IntegerField()
    paid = models.IntegerField()
    discount = models.IntegerField(default=0)
    due = models.IntegerField()
    taxes = models.ManyToManyField(Tax, default=None)
    is_paid_off = models.BooleanField(default=True)


    def is_paid(self):
        return self.get_current() <= 1

    def total_with_taxes(self):
        return self.total + sum([_.get_value(self.total) for _ in self.taxes.all()])

    def get_current(self):
        return self.total_with_taxes() - self.discount - self.paid

    def tax_values(self):
        return [ (_, _.get_value(self.total)) for _ in self.taxes.all() ]

    def due(self):
        current =  self.get_current()
        if current < 1: current = 0
        return current

    def has_due(self):
        return self.due() != 0
