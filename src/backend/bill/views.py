from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.views.generic import DetailView, ListView

from labtest.models import Test

from .models import Bill, Tax
# Create your views here.


class TaxList(ListView):
    queryset = Tax.objects.all().order_by("name")
    template_name = 'taxes.html'
    paginate_by = 10


class TaxCreate(CreateView):
    template_name = 'tax_create.html'
    model = Tax
    fields = ['name', 'method' , 'value']
    success_url = reverse_lazy("taxes")


class TaxUpdate(UpdateView):
    template_name = 'tax_update.html'
    model = Tax
    fields = ['name', 'method', 'value']
    success_url = reverse_lazy("taxes")


class TaxDelete(DeleteView):
    template_name = 'tax_delete_confirm.html'
    model = Tax
    success_url = reverse_lazy('taxes')


def makePaymentForTest(request, test_id):
    if request.method == "POST":
        discount = request.POST.get("discount", 0)
        if discount == '':
            discount = 0
        else: discount = float(discount)
        paid = float(request.POST.get("paid"))
        test = Test.objects.get(id=int(test_id))
        test.bill.discount = discount
        test.bill.paid += paid
        test.bill.paid = round(test.bill.paid, 2)
        test.bill.is_paid_off = test.bill.is_paid()
        test.bill.save()
    return redirect('test', pk=test_id)
