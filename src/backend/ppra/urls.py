"""ppra URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from patient.views import PatientViewSet
from testtype.views import TestCategoryViewSet, TestParticularViewSet, TestParticularGroupViewSet, addTestType, addTestCategory, addTestGroup, TestCategoryDelete, TestGroupDelete, TestTypeDelete, TestCategoryUpdate, TestGroupUpdate, TestTypeUpdate, TestParticularGroupPriceUpdate, TestTypeList
from labtest.views import TestViewSet, TestParticularValueViewSet, PaidTestList, UnpaidTestList
from labtest.views import addTest, TestList, TestDetails, TestDelete, DoctorList, DoctorDelete, DoctorCreate, DoctorUpdate, TestsByDr, TestParticularValueUpdate
from lab.views import LabViewSet, home, labs
from report.views import reports, doctorReport
from bill.views import TaxList, TaxCreate, TaxDelete, TaxUpdate, makePaymentForTest
from jwtauth.views import CreateUserView
from rest_framework import routers
from rest_framework.authtoken import views
from pdf.views import generate_pdf, generate_conjusted_pdf, generate_pdf_without_sign, generate_conjusted_pdf_without_sign, generate_bill
router = routers.DefaultRouter()
router.register(r'patients', PatientViewSet)
router.register(r'test-particular-values', TestParticularValueViewSet)
router.register(r'tests', TestViewSet)
router.register(r'labs', LabViewSet)
router.register(r'test-particular-groups', TestParticularGroupViewSet)
router.register(r'test-categories', TestCategoryViewSet)
router.register(r'test-particulars', TestParticularViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('login/', views.obtain_auth_token),
    path('register/', CreateUserView.as_view()),
    path('reports/', reports, name="reports"),
    path(r'labs/', labs, name="labs"),
    path(r'add-testtype/', addTestType, name="addTestType"),
    path(r'tests/<pk>/', TestDetails.as_view(), name="test"),
    path(r'generate-pdf/<test_id>', generate_pdf, name="generatePDF"),
    path(r'generate-bill/<test_id>', generate_bill, name="generateBill"),
    path(r'generate-pdf-without-sign/<test_id>', generate_pdf_without_sign, name="generatePDFWithoutSign"),
    path(r'generate-conjusted-pdf/<test_id>', generate_conjusted_pdf, name="generateConjustedPDF"),
    path(r'generate-conjusted-pdf-without-sign/<test_id>', generate_conjusted_pdf_without_sign, name="generateConjustedPDFWithoutSign"),
    path(r'add-test/', addTest, name="addTest"),
    path(r'make-payment/<test_id>', makePaymentForTest, name="makePaymentForTest"),
    path(r'', TestList.as_view(), name="tests"),
    path(r'unpaid-tests/', UnpaidTestList.as_view(), name="unpaid-tests"),
    path(r'paid-tests/', PaidTestList.as_view(), name="paid-tests"),
    path(r'taxes/', TaxList.as_view(), name="taxes"),
    path(r'create-tax/', TaxCreate.as_view(), name="create-tax"),
    path(r'tests-by-dr/<dr>', TestsByDr.as_view(), name="tests-by-dr"),
    path(r'reports-by-dr/<dr>', doctorReport, name="reports-by-dr"),
    path(r'tests-by-dr/<dr>/<year>/<month>', TestsByDr.as_view(), name="tests-by-dr"),
    path(r'doctors/', DoctorList.as_view(), name="doctors"),
    path(r'testtypes/', TestTypeList.as_view(), name="testtypes"),
    path(r'add-doctor/', DoctorCreate.as_view(), name="add-doctor"),
    path(r'add-doctor/<pk>', DoctorUpdate.as_view(), name="update-doctor"),
    path(r'delete-tests/<pk>', TestDelete.as_view(), name="delete-test"),
    path(r'delete-doctors/<pk>', DoctorDelete.as_view(), name="delete-doctor"),
    path(r'delete-taxes/<pk>', TaxDelete.as_view(), name="delete-tax"),
    path(r'delete-test-category/<pk>', TestCategoryDelete.as_view(), name="delete-test-category"),
    path(r'delete-test-group/<pk>', TestGroupDelete.as_view(), name="delete-test-group"),
    path(r'delete-test-type/<pk>', TestTypeDelete.as_view(), name="delete-test-type"),
    path(r'add-test-category/', addTestCategory, name="addTestCategory"),
    path(r'update-test-category/<pk>', TestCategoryUpdate.as_view(), name="update-test-category"),
    path(r'update-tax/<pk>', TaxUpdate.as_view(), name="update-tax"),
    path(r'update-test-particular-group-price/<pk>', TestParticularGroupPriceUpdate.as_view(), name="update-test-particular-group-price"),

    path(r'update-test-type/<pk>', TestTypeUpdate.as_view(), name="update-test-type"),
    path(r'update-test-particular-value/<pk>', TestParticularValueUpdate.as_view(), name="update-test-particular-value"),
    path(r'update-test-group/<pk>', TestGroupUpdate.as_view(), name="update-test-group"),
    path(r'add-test-group/', addTestGroup, name="addTestGroup"),
]
