from django.shortcuts import render, redirect
from .models import Report, Bill, AdminReport, TestCountReport
from labtest.models import Test
# Create your views here.

def generate_bill(request, test_id, conjusted=False, signed=True):
    if Test.objects.filter(id=test_id).exists():
        bill = Bill(test_id, signed=signed)
        bill.create()
        return redirect("/static/bill-{}.pdf".format(test_id))



def generate_pdf(request, test_id, conjusted=False, signed=True):
    if Test.objects.filter(id=test_id).exists():
        report = Report(test_id, conjusted=conjusted, signed=signed)
        report.create()
        return redirect("/static/report-{}.pdf".format(test_id))


def generate_pdf_without_sign(request, test_id):
    return generate_pdf(request, test_id, signed=False)

def generate_conjusted_pdf(request, test_id):
    return generate_pdf(request, test_id, True)

def generate_conjusted_pdf_without_sign(request, test_id):
    return generate_pdf(request, test_id, conjusted=True, signed=False)


def generate_adminreport(request, tests, date_from, date_to):
    report = AdminReport(tests, date_from, date_to)
    report.create()
    return redirect("/static/{}".format(report.output_file))



def generate_testcountreport(request, tests, date_from, date_to):
    report = TestCountReport(tests, date_from, date_to)
    report.create()
    return redirect("/static/{}".format(report.output_file))
