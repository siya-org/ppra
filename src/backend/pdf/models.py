from django.db import models
from django.conf import settings
from labtest.models import Test, TestParticularValue
from patient.models import Patient
from lab.models import Lab
from testtype.models import TestCategory, TestParticular, TestParticularGroupPrice, TestParticularGroup
# Create your models here.
from fpdf import FPDF
from fpdf import Template
from pyBSDate import convert_AD_to_BS

class TestCountReport(FPDF):
    def __init__(self, tests, date_from, date_to, signed = True):
        self.tests = tests
        self.date_from = date_from
        self.date_to = date_to
        self.testParticularValues = TestParticularValue.objects.filter(test__in=self.tests)
        self.signed = signed
        super().__init__()
        self.output_file = None
        self.total = 0
        self.total_w_taxes = 0

    def header(self):
        self.image(settings.BASE_DIR + "/static/logo.png", 0, 0, self.w - 2 * self.l_margin, 50)
        self.ln(40)
        self.set_font('Helvetica', 'I', 12)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  "DATE FROM - " + self.date_from.date().isoformat(), ln=1, align="L", border=0)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  "DATE TO - " + self.date_to.date().isoformat(), ln=1, align="L", border=0)
        self.set_font('Helvetica', 'B', 14)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  "TEST", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  "NORMAL", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "ABNORMAL", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "TOTAL", ln=1, align="C", border=1)

    def create(self):
        self.add_page()
        self.set_font('Helvetica', '', 12)

        for group in { _.group for _ in TestParticularValue.objects.filter(test__in=self.tests) }:
            normal_tests = 0
            abnormal_tests = 0
            print(group)
            if group is None or group.default:
                tpv_l = { _.particular for _ in TestParticularValue.objects.filter(group=group)}
                print(tpv_l)
                for tpv in tpv_l:
                    normal_tests = len({ _.test for _ in TestParticularValue.objects.filter(particular=tpv, is_valid=True, group=group)})
                    abnormal_tests = len({ _.test for _ in TestParticularValue.objects.filter(particular=tpv, is_valid=False, group=group)})
                    self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                              tpv.name, ln=0, align="C", border=1)
                    self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                              str(normal_tests), ln=0, align="C", border=1)
                    self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                              str(abnormal_tests), ln=0, align="C", border=1)
                    self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                              str(normal_tests + abnormal_tests), ln=1, align="C", border=1)
            else:
                for test in self.tests:
                    tpvs = list(test.testparticularvalue_set.filter(group=group))
                    if all(( _.is_valid for _ in tpvs )): normal_tests += 1
                    else: abnormal_tests += 1
                self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                          group.name, ln=0, align="C", border=1)
                self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                          str(normal_tests), ln=0, align="C", border=1)
                self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                          str(abnormal_tests), ln=0, align="C", border=1)
                self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                          str(normal_tests + abnormal_tests), ln=1, align="C", border=1)

        parent_dir = getattr(settings, "STATICFILES_DIRS", None)[0]
        self.output_file = "adminreport-{}-{}.pdf".format(self.date_from.date().isoformat(), self.date_to.date().isoformat())
        self.output(parent_dir + self.output_file)
        return self.output_file


class AdminReport(FPDF):
    def __init__(self, tests, date_from, date_to, signed = True):
        self.date_to = date_to
        self.date_from = date_from
        self.tests = tests
        self.signed = signed
        super().__init__()
        self.output_file = None
        self.total = 0
        self.total_w_taxes = 0

    def header(self):
        self.image(settings.BASE_DIR + "/static/logo.png", 0, 0, self.w - 2 * self.l_margin, 50)
        self.ln(40)
        self.set_font('Helvetica', 'I', 12)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  "DATE FROM - " + self.date_from.date().isoformat(), ln=1, align="L", border=0)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  "DATE TO - " + self.date_to.date().isoformat(), ln=1, align="L", border=0)
        self.set_font('Helvetica', 'B', 14)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "DATE", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 1 / 10, 10,
                  "ID", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  "DR. NAME", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "PRICE(Rs.)", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "W/ TAXES(Rs.)", ln=1, align="C", border=1)

    def loadTest(self, test):
        self.set_font('Helvetica', '', 12)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  test.date_requested.date().isoformat(), ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 1 / 10, 10,
                  str(test.id), ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 3 / 10, 10,
                  str(test.ref_by.name), ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  str(test.bill.total), ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  str(test.bill.total_with_taxes()), ln=1, align="C", border=1)

    def create(self):
        self.add_page()
        from itertools import groupby
        for date, tests in groupby(self.tests, lambda t: t.date_requested.date()):
            total = 0
            total_w_taxes = 0
            for test in tests:
                total += test.bill.total
                total_w_taxes += test.bill.total_with_taxes()
                self.loadTest(test)
            self.total += total
            self.total_w_taxes += total_w_taxes
            self.set_font('Helvetica', 'B', 12)
            self.cell((self.w - 2*self.l_margin) * (3/4), 10,
                      "Total", ln=0, align='R')
            self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                      "Rs. " + str(total), ln=0, align='L')
            self.ln(5)
            self.cell((self.w - 2*self.l_margin) * (3/4), 10,
                      "Total Taxes", ln=0, align='R')
            self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                      "Rs. " + str(total_w_taxes - total), ln=0, align='L')
            self.ln(5)
            self.cell((self.w - 2*self.l_margin) * (3/4), 10,
                      "Total W/ Taxes", ln=0, align='R')
            self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                      "Rs. " + str(total_w_taxes), ln=1, align='L')


        self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                  "Grand Total", ln=0, align='R')
        self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                  "Rs. " + str(self.total), ln=0, align='L')
        self.ln(5)
        self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                  "Taxes", ln=0, align='R')
        self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                  "Rs. " + str(self.total_w_taxes - self.total), ln=0, align='L')
        self.ln(5)
        self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                  "Grand Total W/ Taxes", ln=0, align='R')
        self.cell((self.w - 2*self.l_margin) * (1/4), 10,
                  "Rs. " + str(self.total_w_taxes), ln=1, align='L')

        parent_dir = getattr(settings, "STATICFILES_DIRS", None)[0]
        self.output_file = "adminreport-{}.pdf".format(self.tests.first().date_requested.date().isoformat())
        self.output(parent_dir + self.output_file)
        return self.output_file




class Bill(FPDF):
    def __init__(self, test_id, signed = True):
        self.test = Test.objects.get(id=test_id)
        self.signed = signed
        super().__init__()

    def header(self):
        self.image(settings.BASE_DIR + "/static/logo.png", 0, 0, self.w - 2 * self.l_margin, 50)

        self.ln(40)
        self.set_font('Helvetica', 'B', 14)
        self.cell((self.w - 2*self.l_margin) / 2, 10,
                  "PATIENT ID - " + str(self.test.id), ln=1, align='L')
        self.cell((self.w - 2*self.l_margin) / 2, 10,
                  "PATIENT NAME - " + self.test.patient.name.upper(), ln=0, align='L')
        self.cell((self.w - 2*self.l_margin) / 2, 10,
                  "DATE - " + self.test.date_requested.date().isoformat(), ln=1, align='R')
        self.cell(10, 10,
                  "S.N.", ln=0, align="C", border=1)
        self.cell(((self.w - 2*self.l_margin)* 4 / 10) - 10, 10,
                  "Test Particular", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "Quantity", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "Rate (Rs.)", ln=0, align="C", border=1)
        self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                  "Amount (Rs.)", ln=1, align="C", border=1)

    def total(self):
        self.cell(((self.w - 2*self.l_margin)* 8 / 10), 10,
                  "Total", ln=0, align="R", border=1)
        self.cell(((self.w - 2*self.l_margin)* 2 / 10), 10,
                  str(self.test.bill.total), ln=1, align="R", border=1)
        for each in self.test.bill.tax_values():
            self.cell(((self.w - 2*self.l_margin)* 8 / 10), 10,
                      each[0].name + " " + each[0].get_value_for_display(), ln=0, align="R", border=1)
            self.cell(((self.w - 2*self.l_margin)* 2 / 10), 10,
                      str(each[1]), ln=1, align="R", border=1)
        self.cell(((self.w - 2*self.l_margin)* 8 / 10), 10,
                  "Total With Tax", ln=0, align="R", border=1)
        self.cell(((self.w - 2*self.l_margin)* 2 / 10), 10,
                  str(self.test.bill.total_with_taxes()), ln=1, align="R", border=1)
        self.cell(((self.w - 2*self.l_margin)* 8 / 10), 10,
                  "Discount", ln=0, align="R", border=1)
        self.cell(((self.w - 2*self.l_margin)* 2 / 10), 10,
                  str(self.test.bill.discount), ln=1, align="R", border=1)

        self.cell(((self.w - 2*self.l_margin)* 8 / 10), 10,
                  "Payment", ln=0, align="R", border=1)
        self.cell(((self.w - 2*self.l_margin)* 2 / 10), 10,
                  str(self.test.bill.paid), ln=1, align="R", border=1)

        self.cell(((self.w - 2*self.l_margin)* 8 / 10), 10,
                  "Due", ln=0, align="R", border=1)
        self.cell(((self.w - 2*self.l_margin)* 2 / 10), 10,
                  str(self.test.bill.due()), ln=1, align="R", border=1)

    def testData(self):
        count = 1
        group_list = {_.group for _ in TestParticularValue.objects.filter(test=self.test) if _.group is not None}

        for group in group_list:
            tps = list(TestParticularValue.objects.filter(test=self.test,group=group))
            quantity = len(tps)
            rate = sum([ _.get_price() for _ in tps ]) / quantity
            amount = quantity * rate
            self.cell(10, 10,
                      str(count), ln=0, align="C", border="LR")
            self.cell(((self.w - 2*self.l_margin)* 4 / 10) - 10, 10,
                      group.name, ln=0, align="C", border="LR")
            self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                      str(quantity), ln=0, align="C", border="LR")
            self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                      str(rate), ln=0, align="C", border="LR")
            self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                      str(amount), ln=1, align="C", border="LR")
            count += 1

        for tp in list(TestParticularValue.objects.filter(test=self.test, group=None)):
            quantity = 1
            rate = tp.get_price()
            amount = quantity * rate
            self.cell(10, 10,
                      str(count), ln=0, align="C", border="LR")
            self.cell(((self.w - 2*self.l_margin)* 4 / 10) - 10, 10,
                      tp.particular.acronym, ln=0, align="C", border="LR")
            self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                      str(quantity), ln=0, align="C", border="LR")
            self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                      str(rate), ln=0, align="C", border="LR")
            self.cell((self.w - 2*self.l_margin)* 2 / 10, 10,
                      str(amount), ln=1, align="C", border="LR")
            count += 1
    def initials(self):
        self.ln()
        if self.signed:
            self.image(settings.BASE_DIR + "/static/sig.jpg", (self.w - 2 * self.l_margin) - 10, self.get_y(), 20)
        self.ln()
        self.line(self.w  - self.l_margin - 25, self.get_y(), self.w-self.l_margin, self.get_y())

        self.set_font('Helvetica', 'B', 10)

        self.cell((self.w - 2*self.l_margin), 10,
                  "Binit Pandey", ln=0,border=0, align="R")
        self.ln(3)
        self.set_font('Helvetica', 'I', 10)
        self.cell((self.w - 2*self.l_margin), 10,
                  "Medical Technologist", ln=0,border=0, align="R")
        self.ln(3)
        self.set_font('Helvetica', 'B', 8)
        self.cell((self.w - 2*self.l_margin), 10,
                  "NHPC Reg. No. A240", ln=1,border=0, align="R")
        if self.test.bill.is_paid():
            self.image(settings.BASE_DIR + "/static/paid.png", (self.w - 2*self.l_margin) - 30, self.get_y(), 30, 30)

    def create(self):
        self.add_page()
        self.testData()
        self.total()
        self.initials()

        parent_dir = getattr(settings, "STATICFILES_DIRS", None)[0]
        output_file = parent_dir + "bill-{}.pdf".format(str(self.test.id))
        self.output(output_file)
        return output_file


class Report(FPDF):
    def __init__(self, test_id, conjusted = False, signed = True):
        self.in_category = False          # False = not in category, True = in category
        self.current_category = None
        if conjusted: format = (210, 297/2)
        else: format = 'A4'
        self.signed = signed
        super().__init__(format=format)
        self.test = Test.objects.get(id=test_id)
        self.lab = Lab.objects.all()[0]
        self.arr = (("name", 6, 20, 'L'), ("unit", 3, 20, 'L'), ("method", 2, 20, 'L'), ("test value", 5, 20, 'C'), ("ref. value", 4, 20, 'L'))
        self.tests_left = True

    def create(self):
        self.set_top_margin(2)
        self.add_page()
        for category in {_.particular.category for _ in TestParticularValue.objects.filter(test=self.test)}:
            self.addCategoryValue(category)
        self.tests_left = False
        self.initials()
        parent_dir = getattr(settings, "STATICFILES_DIRS", None)[0]
        output_file = parent_dir + "report-{}.pdf".format(str(self.test.id))
        self.output(output_file)
        return output_file

    def header(self):
        self.image(settings.BASE_DIR + "/static/logo.png", (self.w - 2 * self.l_margin) - 80, 0, 90)
        self.loadPatientData()
        self.set_font('Helvetica', 'B', 9)
        if self.page_no() > 1 and self.in_category:
            self.set_font('Helvetica', 'B', 10)
            r, g, b = tuple( map(lambda l: int(self.current_category.colour[1:][l:l+2], 16), range(0, 6, 2) ) )
            self.set_fill_color(r, g, b)
            if (r*0.299 + g*0.587 + b*0.114) > 186:
                self.set_text_color(0, 0, 0)
            else:
                self.set_text_color(0, 0, 0)
            self.cell(self.w - 2*self.l_margin, 5,
                      self.current_category.name.upper(), ln=0, fill=True, align='C')
            self.ln(3)

            for field in self.arr:
                self.cell((self.w - 2*self.l_margin) * field[1] / field[2], 10,
                          field[0].upper(), ln=0,border=0, align=field[3])
            self.ln(5)


    def loadPatientData(self):
        self.set_font('Helvetica', '', 10)
        self.cell((self.w - 2*self.l_margin)/3.5, 4,
                  "Name - " + self.test.patient.name,
                  border=0, ln=0, align='L')
        self.cell((self.w - 2*self.l_margin)/3.5, 4,
                  "PATIENT ID - " + str(self.test.patient.id),
                  border=0, ln=1, align='L')
        date = self.test.date_requested.date()
        nepali_date = convert_AD_to_BS(date.year, date.month, date.day)
        f_nepali_date = "{}-{}-{}".format(nepali_date.year, nepali_date.month, nepali_date.day)
        self.cell((self.w - 2*self.l_margin)/3.5, 4,
                  "Date - {} ({})".format(f_nepali_date, date.isoformat()),
                  border=0, ln=0, align="L")
        self.cell((self.w - 2*self.l_margin)/3.5, 4,
                  "Age / Sex - " + str(self.test.patient.age) + "/" + self.test.patient.sex,
                  border=0, ln=1)
        self.cell((self.w - 2*self.l_margin)/3.5, 4,
                  "Address - " + self.test.patient.address,
                  border=0, ln=0, align='L')
        self.cell((self.w - 2*self.l_margin)/3.5, 4,
                  "Ref. By - " + str(self.test.ref_by),
                  border=0, ln=1, align='L')
        self.multi_cell((self.w - 2*self.l_margin)/2, 4,
                  "Investigation Req. - " + self.test.investigation_required,
                        border=0, align='L')
        self.set_y(30)
        self.set_line_width(1)
        self.set_draw_color(0, 0, 255)
        self.line(self.l_margin, self.get_y() + 2, self.w - self.l_margin, self.get_y() + 2)
        self.ln(3)
        self.set_font('Helvetica', 'B', 11)
        self.set_text_color(0, 0, 255)
        self.cell(self.w - 2 * self.l_margin, 5,
                   "Laboratory Reporting Form",
                  border=0, ln=1, align="C")

        self.line(self.l_margin, self.get_y() + 0.5, self.w - self.l_margin, self.get_y() + 0.5)

        self.set_line_width(.2)
        self.set_text_color(0, 0, 0)
        self.set_draw_color(0, 0, 0)

    def addCategoryValue(self, category):
        self.in_category = True
        self.current_category = category
        self.ln(1)
        self.set_font('Helvetica', 'B', 12)
        r, g, b = tuple( map(lambda l: int(category.colour[1:][l:l+2], 16), range(0, 6, 2) ) )
        self.set_fill_color(r, g, b)
        if (r*0.299 + g*0.587 + b*0.114) > 186:
            self.set_text_color(0, 0, 0)
        else:
            self.set_text_color(0, 0, 0)
        self.cell(self.w - 2*self.l_margin, 5,
                  category.name.upper(), ln=0, fill=True, align='C')
        self.ln(3)
        particulars = TestParticularValue.objects\
                                         .filter(test=self.test)\
                                         .filter(particular__category=category)
        groups = { _.group for _ in particulars }
        groups = [ {"group": group, "particulars": particulars.filter(group=group)} for group in groups ]
        self.set_text_color(0, 0, 0)
        self.set_fill_color(255, 255, 255)
        self.set_font('Helvetica', 'B', 12)
        for field in self.arr:
            self.cell((self.w - 2*self.l_margin) * field[1] / field[2], 10,
                      field[0].upper(), ln=0,border=0, align=field[3])
        self.ln(5)
        self.set_font('Helvetica', '', 12)
        for group in groups:
            border = 0
            if group['group']:
                border="L"
                self.set_font('Helvetica', 'UI', 12)
                self.cell((self.w - 2*self.l_margin), 10,
                          group['group'].name, ln=0,border=0, align="L")
                self.ln(5)
                self.set_font('Helvetica', '', 12)

            for testParticularValue in group['particulars']:
                self.cell((self.w - 2*self.l_margin) * (self.arr[0][1] / self.arr[0][2]), 10,
                          "{} ({})".format(testParticularValue.particular.name, testParticularValue.particular.acronym), ln=0,border=border, align="L")
                self.cell((self.w - 2*self.l_margin) * (self.arr[1][1] / self.arr[1][2]), 10,
                          testParticularValue.particular.unit, ln=0,border=0, align="L")
                self.cell((self.w - 2*self.l_margin) * (self.arr[2][1] / self.arr[2][2]), 10,
                          testParticularValue.particular.method, ln=0,border=0, align="L")
                if (not testParticularValue.is_valid):
                    self.set_text_color(255, 0, 0)
                self.set_font('Helvetica', 'B', 12)
                if (testParticularValue.particular.get_ref_type() == "multilist"):
                    x, y = self.get_x(), self.get_y()
                    for val in testParticularValue.value.split(","):
                        self.set_xy(x, self.get_y())
                        self.cell((self.w - 2*self.l_margin) * (self.arr[3][1] / self.arr[3][2]), 10,
                                  val, ln=0,border=0, align="C")
                        self.ln(5)
                    self.set_xy(x + (self.w - 2*self.l_margin) * (self.arr[3][1] / self.arr[3][2]), self.get_y() - 3 * len(testParticularValue.value.split(",")))
                else:
                    self.cell((self.w - 2*self.l_margin) * (self.arr[3][1] / self.arr[3][2]), 10,
                              testParticularValue.get_value(), ln=0,border=0, align="C")
                self.set_text_color(0, 0, 0)
                self.set_font('Helvetica', '', 12)
                if (testParticularValue.particular.get_ref_type() != "multilist"):
                    self.cell((self.w - 2*self.l_margin) * (self.arr[4][1] / self.arr[4][2]), 10,
                              testParticularValue.get_ref(), ln=0,border=0, align="L")
                if (testParticularValue.particular.get_ref_type() == "multilist"):
                    self.set_xy(self.get_x(), self.get_y() +  3 * len(testParticularValue.value.split(",")))
                    self.ln(1)
                else:
                    self.ln(7)
            if group['group']:
                self.dashed_line(self.l_margin, self.get_y() + 5, self.w-self.l_margin, self.get_y() + 5)
                self.set_y(self.get_y() + 5)
        self.in_category = False
        self.current_category = None

        self.ln(5)
        self.line(self.l_margin, self.get_y(), self.w-self.l_margin, self.get_y())

    def footer(self):
        if self.tests_left:
            self.set_y(-25)
            self.initials()

    def initials(self):
        if self.signed:
            self.image(settings.BASE_DIR + "/static/sig.jpg", (self.w - 2 * self.l_margin) - 10, self.get_y(), 20)
        self.ln()
        self.line(self.w  - self.l_margin - 25, self.get_y(), self.w-self.l_margin, self.get_y())

        self.set_font('Helvetica', 'B', 10)

        self.cell((self.w - 2*self.l_margin), 10,
                  "Binit Pandey", ln=0,border=0, align="R")
        self.ln(3)
        self.set_font('Helvetica', 'I', 10)
        self.cell((self.w - 2*self.l_margin), 10,
                  "Medical Technologist", ln=0,border=0, align="R")
        self.ln(3)
        self.set_font('Helvetica', 'B', 8)
        self.cell((self.w - 2*self.l_margin), 10,
                  "NHPC Reg. No. A240", ln=0,border=0, align="R")
