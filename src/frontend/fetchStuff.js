/*
  SOME GLOBAL CONSTANTS
*/

const LAB_FIELDS = [
	"name",
	"pan_no",
	"description",
	"reg_no",
	"council_reg_no",
	"mold_reg_no",
	"moto" ];

const PATIENT_FIELDS = [
	"name", "age", "sex", "patient_id", "address"
];
// CONSTANTS OVER

function logout() {
	localStorage.removeItem('token');
	location.reload();
}

function login() {
	username = document.getElementById('user-username').value;
	password = document.getElementById('user-password').value;
	getLoginToken(username, password).then(data => {
		localStorage.setItem('token', data.token);
		localStorage.setItem('username', username);
		location.reload();
	});

}

function checkLogin() {
	function getText() {
		userDetails = "Hello, <span class='user-name'></span><button onclick='logout();' class='btn btn-warning'>Logout</button>";
		loginForm = `<form class="form-inline"><div class="form-group mb-2"><input type="text" class="form-control" id="user-username" placeholder="Username"></div><div class="form-group mx-sm-3 mb-2"><input type="password" class="form-control" id="user-password" placeholder="Password"></div> <button onclick="login();" type="button" class="btn btn-primary mb-2">Confirm identity</button></form>`;
		token = localStorage.getItem('token');
		if (token == "" || token == null) {
			$(".container-body").hide();
			return loginForm
		};
		$(".container-body").show();
		return userDetails;
	}

	Array.from(document.getElementsByClassName("login-or-not")).map(el => el.innerHTML=getText());
	Array.from(document.getElementsByClassName("user-name")).map(el => el.innerText=localStorage.getItem('username'));
}

async function callFetchWithAuth(url, options) {
    auth_token = localStorage.getItem("token");
    if (auth_token == "") return null;

    if (options && 'headers' in options) {
		options.headers['Authorization'] = 'Token ' + auth_token;
    } else {			// if no headers are specified
		if (options == null) options = new Object();
		options.headers = {
			'Content-Type': 'application/json',
			'Authorization': 'Token ' + auth_token,
		};
    }
    const response = await fetch(url, options);
    return await response;
}


async function getLoginToken(username, password) {
    fetch_options = {
		headers: { "Content-Type" : "application/json" },
		method: "POST",
		body: JSON.stringify({ username: username, password: password })
    };
    const response = await fetch("/api/login/", fetch_options);
    return await response.json();
}


/*

  LAB STUFF

*/

async function getAllLabs() {
    const response = await callFetchWithAuth("/api/labs/");
    return await response.json();
}

async function addNewLab(data) {
	const response = await callFetchWithAuth("/api/labs/", { method: "POST", body: JSON.stringify(data) });
	return await response.json();
}

async function getLab(id) {
    const response = await callFetchWithAuth("/api/labs/" + id.toString() + "/");
    return await response.json();
}

async function patchLab(id, data) {
    const response = await callFetchWithAuth("/api/labs/" + id.toString() + "/", { method: "PATCH", body: JSON.stringify(data)});
    return await response.json();
}

function populateLabData(data) {
    for (field in data)  {
		Array.from(document.getElementsByClassName("lab-" + field)).map(el => el.innerText = data[field]);
    }
}

$(document).ready(() => {getLab(1).then(populateLabData); checkLogin();});

// getLab(1).then(populateLabData); // populate page with lab details


// LAB STUF COMPLETE


/*

  PATIENT STUFF

*/
async function getAllPatients() {
    const response = await callFetchWithAuth("/api/patients/");
    return await response.json();
}

async function addNewPatient(data) {
    const response = await callFetchWithAuth("/api/patients/", { method: "POST", body: JSON.stringify(data)});
	console.log(response);
    return await response.json();
}
async function patchPatient(id, data) {
    const response = await callFetchWithAuth("/api/patients/" + id.toString() + "/", { method: "PATCH", body: JSON.stringify(data)});
    return await response.json();
}

async function deletePatient(id) {
    return await callFetchWithAuth("/api/patients/" + id.toString() + "/", { method: "DELETE" });
}




/*
  TEST TYPE STUFF
*/

async function getAllTestCategories() {
    const response = await callFetchWithAuth("/api/test-categories/");
    return await response.json();
}

async function getAllTestParticulars() {
    const response = await callFetchWithAuth("/api/test-particulars/");
    return await response.json();
}
async function addNewTestCategory(name) {
    const response = await callFetchWithAuth("/api/test-categories/", { method: "POST", body:JSON.stringify({ "name": name })});
	console.log(response);
    return await response.json();
}
async function addNewTestParticular(name, unit, ref_value, price, category_id) {
	body = {
		"name": name,
		"unit": unit,
		"ref_value": ref_value,
		"category": category_id,
		"ref_type": "range",
		"value_type": "number",
		"price": price
	};
	console.log(body);
    const response = await callFetchWithAuth("/api/test-particulars/", { method: "POST", body:JSON.stringify(body)});
	console.log(response);
    return await response.json();
}

async function deleteTestCategory(id) {
	return await callFetchWithAuth("/api/test-categories/" + id.toString() + "/", { method: "DELETE" });
}
async function deleteTestParticular(id) {
    return await callFetchWithAuth("/api/test-particulars/" + id.toString() + "/", { method: "DELETE" });
}

/*

  Test Stuff

*/

async function getAllTestParticularValues(){
    const response = await callFetchWithAuth("/api/test-particular-values/");
    return await response.json();
}
async function addNewTestParticularValue(data) {
    const response = await callFetchWithAuth("/api/test-particular-values/", { method: "POST", body:JSON.stringify(data)});
    return await response.json();

}
async function updateTestParticularValue(id, data){
    const response = await callFetchWithAuth("/api/test-particular-values/" + id.toString() + "/", { method: "PATCH", body: JSON.stringify(data)});
    return await response.json();
}

async function deleteTestParticularValue(id){
    return await callFetchWithAuth("/api/test-particular-values/" + id.toString() + "/", { method: "DELETE" });
}

async function getAllTests(){
    const response = await callFetchWithAuth("/api/tests/");
    return await response.json();
}
async function addNewTest(data) {
    const response = await callFetchWithAuth("/api/tests/", { method: "POST", body:JSON.stringify(data)});
    return await response.json();
}
async function deleteTest(id){
    return await callFetchWithAuth("/api/tests/" + id.toString() + "/", { method: "DELETE" });
}
async function updateTest(id, data){
    const response = await callFetchWithAuth("/api/tests/" + id.toString() + "/", { method: "PATCH", body: JSON.stringify(data)});
    return await response.json();
}
