FROM python:3.7-alpine


COPY src/frontend /webapp
COPY src/backend /ppra
COPY requirements.txt install_assets.sh start.sh /ppra/

WORKDIR /ppra

COPY nginx/ppra.conf /etc/nginx/conf.d/default.conf
RUN sh install_assets.sh

CMD ["sh", "start.sh"]
