#!/usr/bin/env sh
nginx
gunicorn --workers 3 \
	 --bind=unix:/tmp/ppra.sock ppra.wsgi --chdir /ppra
