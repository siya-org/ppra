#!/usr/bin/env sh
apk add --no-cache jpeg-dev zlib-dev nginx
apk add --no-cache --virtual .build-deps build-base linux-headers
pip install -r requirements.txt
apk del .build-deps

mkdir /run/nginx			# this is needed to run nginx
./manage.py collectstatic
./manage.py makemigrations
./manage.py migrate

export DJANGO_SUPERUSER_PASSWORD="a"

mv /ppra/logo.jpg /ppra/static

./manage.py createsuperuser --username ays --email ayushjha@pm.me

apk add --no-cache --virtual .build-deps unzip

wget -O bootstrap.zip "https://github.com/twbs/bootstrap/releases/download/v4.4.1/bootstrap-4.4.1-dist.zip"

unzip bootstrap.zip
mv bootstrap-4.4.1-dist /ppra/static/bootstrap
apk del .build-deps
